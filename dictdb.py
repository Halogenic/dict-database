
import copy
import re
import sys

from collections import defaultdict, namedtuple


class OperationalError(Exception):
	pass


class SchemaError(Exception):
	pass


if sys.version_info >= (2, 7):
	from collections import OrderedDict
else:
	class OrderedDict(dict):
		def __init__(self, *args, **kwargs):
			super(OrderedDict, self).__init__(*args, **kwargs)

			self._key_order = []

			if args and isinstance(args[0], (list, tuple, set)):
				for key, _ in args[0]:
					self._key_order.append(key)
			elif args and isinstance(args[0], dict):
				self._key_order = args[0].keys()

		def keys(self):
			return copy.copy(self._key_order)

		def values(self):
			return [self[key] for key in self._key_order]

		def items(self):
			return [(key, self[key]) for key in self._key_order]

		def iterkeys(self):
			return (key for key in self._key_order)

		def iteritems(self):
			return ((key, self[key]) for key in self._key_order)

		def __setitem__(self, key, value):
			super(OrderedDict, self).__setitem__(key, value)
			self._key_order.append(key)

		def __delitem__(self, key):
			super(OrderedDict, self).__delitem__(key)
			self._key_order.remove(key)

		def __eq__(self, other):
			return self.values() == other.values()

		def __hash__(self):
			return hash(tuple(self.values()))


class Row(object):
	"""
	TODO: Optimise to not create a new namedtuple class on creation,
	make it into its own optimised tuple/dict like object.
	"""

	def __init__(self, items):
		self._dict = OrderedDict(items)

	def keys(self):
		return tuple(self._dict.keys())

	def get(self, key, default=None):
		return self._dict.get(key, default)

	def __iter__(self):
		return iter(self._dict.values())

	def __getitem__(self, key):
		return self._dict[key]

	def __eq__(self, other):
		return self._dict == other._dict

	def __repr__(self):
		return repr(self._dict)

	def __hash__(self):
		return hash(self._dict)

class Row(object):
	"""
	Optimised row that just requires the row dict and fields
	wanted to be passed in and processes those to display it
	correctly to user.
	"""

	def __init__(self, row, fields):
		self._row = row
		self._fields = fields

	def keys(self):
		return self._fields

	def get(self, key, default=None):
		return self._dict.get(key, default)

	def __iter__(self):
		return (self._row[field] for field in fields)

	def __getitem__(self, key):
		if key in self._fields:
			return self._row[key]
		else:
			raise KeyError(str(key))

	def __eq__(self, other):
		return self._row == other._row

	def __repr__(self):
		return repr(dict([(field, self._row[field]) for field in self._fields]))

	def __len__(self):
		return len(self._fields)

	def __hash__(self):
		return hash(self._row)


class Key(object):

	def __init__(self, value):
		self.value = value

	def as_tuple(self):
		if isinstance(self.value, (list, tuple, set)):
			return tuple(self.value)

		return (self.value, )

	def __str__(self):
		return str(self.as_tuple())

class Table(object):

	def __init__(self, name, schema):
		self.name = name
		self.schema = copy.deepcopy(schema)

		# convert user-defined schema of fields into OrderedDict
		fields = OrderedDict([(field["name"], field) for field in self.schema["fields"]])

		self.schema["fields"] = fields

		self.key = Key(self.schema["primary_key"])

		self._rows = []

		self._indexes = []
		self._indexed = {}

		self.add_index(self.key.value)
		for index in schema.get("indexes", []):
			self.add_index(index, ignore=True)

	def fetch(self, filter, fields=None):
		"""
		Return the records matching the specified filters.

		:Parameters:
			filter : filters rows to be fetched, if this evaluates to False all rows are selected
			fields : list of strings of fields to be returned
		"""

		fields = fields or self.schema["fields"].keys()

		result_views = []

		if filter:
			# perform a select over the rows in the database
			# only supports AND
			for key, value in filter.items():
				key = Key(key)

				# use set logic to filter the records down
				if key.as_tuple() in self._indexes:
					results = set([Row(row, fields) for row in self._indexed[key.as_tuple()].get(value, [])])
					# results = set([Row([(key, value) for key, value in row.iteritems() if key in fields]) for row in self._indexed[key.as_tuple()].get(value, [])])
				else:
					pass
					# results = set([Row([(key, value) for key, value in row.iteritems() if key in fields]) for row in self._rows if row[key.value] == value])

				if results:
					result_views.append(results)

			if not result_views:
				return []
		else:
			pass
			# get all items
			# result_views = [set([Row([(key, value) for key, value in row.iteritems() if key in fields]) for row in self._rows])]

		records = list(reduce(lambda x,y: x.intersection(y), result_views))

		return records

	def insert(self, data):
		"""
		Insert a record into the table.

		TODO: could avoid lots of processing at select/query time if we do all
		the processing at insert time (creation of immutable objects representing rows etc. so we copy the data
		across instead of simply updating it in a dictionary)

		:Parameters:
			data : dict of data to insert into the table
		"""

		if not set(data.keys()).issubset(self.schema["fields"].keys()):
			raise SchemaError("Given record does not match table schema.")

		if not set(self.key.as_tuple()).issubset(data.keys()):
			raise SchemaError("Given record does not contain value(s) for primary key '%s'." % str(self.key.value))

		row = OrderedDict([(field, data.get(field)) for field in self.schema["fields"].keys()])
		self._rows.append(row)

		# add to any indexed columns
		for index_key in self._indexes:
			self._indexed[index_key].setdefault(data[index_key[0]], []).append(row)

	def update(self, filter, data):
		"""
		Update a record in the table with the given unique key.

		:Parameters:
			filter : for filtering the list of rows to be updated
			data : dict mapping field names to the values to be updated
		"""

		records = self.fetch(filter, fields=self.key.as_tuple())

	def delete(self, filter):
		"""
		Delete the records returned by 'filter'.

		:Parameters:
			filter : the filter determining what records will be deleted
		"""

		records = self.fetch(filter)

		for record in records:
			# remove from indexed locations
			for index_key in self._indexes:
				# TODO: will need to convert record index keys
				# into compatible tuples because keys can be
				# tuples of fields not just one field

				self._indexed[record[index_key]].remove(record)

			# TODO: speed up by getting index of each record
			# to delete so that we can delete it directly in the rows list

			# will remove first dict with same values contained
			self._rows.remove(record)

	def add_index(self, key, ignore=False):
		"""
		Create an index on the table with the given key.

		In a key the order matters and it should be given as either
		a single value or a tuple/list of field names.
		"""

		key = Key(key)

		if key.as_tuple() in self._indexed:
			if ignore:
				return

			raise SchemaError("Index already exists on table: %s" % key.value)

		key_tuple = key.as_tuple()

		self._indexes.append(key_tuple)

		self._indexed[key_tuple] = {}

		# now apply the key to all existing records
		for record in self._rows:
			self._indexed[key_tuple].setdefault(record[key_tuple[0]], []).append(record)

	def get(self, key, fields=None):
		"""
		Convenience method for returning a record with
		the given primary key.
		"""

		fields = fields or self.schema["fields"].keys()

		try:
			record = self._indexed[key]
		except KeyError:
			raise OperationalError("Record with key %s not found." % str(key))

		return dict([(field, record[field]) for field in fields])

	def __getitem__(self, key):
		return self.get(key)

	def __delitem__(self, key):
		self.delete({ self.key["name"]: key })

	def _create_filter_fn(self, filter_dict):
		def fn(records):
			pass

		return fn


class DictDB(object):
	
	def __init__(self):
		self._tables = {}

	def create_table(self, name, schema):
		"""
		Create a table in the database with given name and schema.

		The schema is a simple list of dicts which store the table's
		field data.

		schema = {
			"fields": [
				{
					"name": "id",
					"type": int,
					"constraints": ["AUTOINCREMENT", "NOT NULL"]
				},
				{
					"name": "name",
					"type": str,
					"constraints": ["UNIQUE"]
				}
			],
			"primary_key": ["id"], # primary keys are automatically unique
			"foreign_keys": [],
			"indexes": [("id", "name")] # any indexes to be applied immediately (these can be added later)
		}

		TODO: primary_key should support compound key

		:Parameters:
			 name : name of the table
			 schema : table schema
		"""

		if name in self._tables:
			raise OperationalError("Table %s already exists." % name)

		if not re.match(r"^\w+$", name):
			raise OperationalError("Invalid table name '%s'" % name)

		self._tables[name] = Table(name, schema)

	def delete_table(self, name):
		"""
		Delete the specified table.
		"""

		try:
			del self._tables[name]
		except KeyError:
			raise OperationalError("Table %s does not exist" % name)

	def add_index(self, table_name, key):
		"""
		Create an index on 'table' with the given key.
		"""
		
		if table_name not in self._tables:
			raise OperationalError("Table %s does not exist." % table_name)

		table = self._tables[table_name]
		table.add_index(key)

	def fetch(self, table_name, filter, fields=None):
		if table_name not in self._tables:
			raise OperationalError("Table %s does not exist." % table_name)

		table = self._tables[table_name]
		return table.fetch(filter, fields)

	def insert(self, table_name, data):
		if table_name not in self._tables:
			raise OperationalError("Table %s does not exist." % table_name)

		table = self._tables[table_name]
		table.insert(data)

	def update(self, table_name, filter, data):
		if table_name not in self._tables:
			raise OperationalError("Table %s does not exist." % table_name)

		table = self._tables[table_name]
		table.update(filter, data)

	def delete(self, table_name, filter):
		if table_name not in self._tables:
			raise OperationalError("Table %s does not exist." % table_name)

		table = self._tables[table_name]
		table.delete(filter)

db = DictDB()

schema = {
	"fields": [
		{
			"name": "id",
			"type": int,
			"constraints": ["AUTOINCREMENT", "NOT NULL"]
		},
		{
			"name": "name",
			"type": str,
			"constraints": ["UNIQUE"]
		},
		{
			"name": "random",
			"type": None # can store arbitrary data that isn't integrity checked
		}
	],
	"primary_key": "id", # primary keys are automatically unique
	"foreign_keys": [],
	"indexes": [] # any indexes to be applied immediately (these can be added later)
}

db.create_table("test", schema)

db.insert("test", { "id": 0, "name": "something" })
db.insert("test", { "id": 1, "name": "something" })

import spider

import time
sh = spider.getHandler()

def load_table(table):
	sql_schema = sh.dataResultQuery("DESCRIBE %s" % table)
	indexes = sh.dataResultQuery("SHOW INDEX FROM %s" % table)

	schema = { "fields": [] }

	for record in sql_schema:
		field = {
			"name": record["Field"],
			"type": str,
			"constraints": []
		}

		schema["fields"].append(field)

		if record["Key"] == "PRI":
			schema["primary_key"] = record["Field"]

	schema["indexes"] = [record["Column_name"] for record in indexes]

	db.create_table(table, schema)

	# insert all data
	records = sh.dataResultQuery("SELECT * FROM %s" % table)

	for record in records:
		db.insert(table, record)